# getGeoJsonDatasFromPostGIS

Very simple PHP Scripts to retrieve datas in JSON or GeoJSON format from a PostgreSQL/PostGIS db tables.

# Example
* This url retrieve departements of Auvergne-Rhône-Alpes (french region) as GeoJSON from `webgis.departements_aura` table : 

    http://localhost/~fcloitre/getdatas/getData.php?geotable=webgis.departements_aura&geomfield=geom&fields=code_dept,nom_dept

* This url retrieve departements of Auvergne-Rhône-Alpes (french region) as a simple JSON format, datas are under `datas` key:
    
    http://localhost/~fcloitre/getdatas/getData.php?table=webgis.departements_aura&geomfield=geom&fields=code_dept,nom_dept